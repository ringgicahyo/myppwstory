from django.shortcuts import render

# Create your views here.


def homepage_view(request):
    return render(request, "Home.html")


def blog_view(request):
    return render(request, "Blog.html")


def experiences_view(request):
    return render(request, "Experiences.html")


def about_view(request):
    return render(request, "About.html")


def contact_view(request):
    return render(request, "Contact.html")


def guestbook_view(request):
    return render(request, "Guest.html")
