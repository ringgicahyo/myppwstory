from django.urls import path
from .views import *

urlpatterns = [
    path('home/', homepage_view, name='home'),
    path('blog/', blog_view, name='blog'),
    path('exp/', experiences_view, name='exp'),
    path('about/', about_view, name='about'),
    path('contact/', contact_view, name='contact'),
    path('guest/', guestbook_view, name='guest'),
    path('', homepage_view, name='')
]
