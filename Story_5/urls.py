from django.urls import path
from .views import *

urlpatterns = [
    path('schedule/', schedule, name='schedule'),
    path('post/', formView, name='post'),
    path('activity/', form_result, name='activity'),
    path('delete/', delete_database, name='delete')
]