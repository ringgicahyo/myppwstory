from django.db import models

# Create your models here.
class modelBase(models.Model):
    date = models.CharField(max_length=50)
    activity = models.CharField(max_length=30)
    location = models.CharField(max_length=30)
    category = models.CharField(max_length=30)