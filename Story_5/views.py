from django.shortcuts import render
from .forms import formPage
from .models import modelBase
from django.http import HttpResponseRedirect
from datetime import datetime

# Create your views here.
def formView(request):
    response = {}
    sched = formPage(request.POST or None)
    if (request.method == 'POST' and sched.is_valid()):
        response['date'] = request.POST['date']
        response['activity'] = request.POST['activity']
        response['location'] = request.POST['location']
        response['category'] = request.POST['category']
        dt = datetime.strptime(response['date'], '%Y-%d-%mT%H:%M')
        schedule_form = modelBase(date=dt.strftime('%A, %B %d %Y at %I:%M %p'),
                            activity=response['activity'],
                            location=response['location'],
                            category=response['category'])
        schedule_form.save()
        return HttpResponseRedirect('/activity/')
    else:
        return HttpResponseRedirect('/schedule/')

def schedule(request):
    response = {'form': formPage}
    return render(request, "Forms.html", response)

def form_result(request):
    result = modelBase.objects.all()
    response = {'result': result}
    html = 'FormResult.html'
    return render(request, html, response)

def delete_database(request):
    modelBase.objects.all().delete()
    return HttpResponseRedirect('/activity/')
