from django import forms

class formPage(forms.Form):

    date = forms.DateTimeField(required=True, widget=forms.DateTimeInput(attrs={'type': 'datetime-local'}), input_formats=['%Y-%d-%mT%H:%M'])
    activity = forms.CharField(label='Activity:', max_length=30, required=True, widget=forms.TextInput(attrs={'class': 'form-control'}))
    location = forms.CharField(label='Location:', max_length=30, required=True, widget=forms.TextInput(attrs={'class': 'form-control'}))
    category = forms.CharField(label='Category:', max_length=30, required=True, widget=forms.TextInput(attrs={'class': 'form-control'}))

